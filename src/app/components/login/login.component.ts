import { Component, OnInit } from '@angular/core';
import { Form, FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { IUser } from 'src/app/domain/iuser';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
form: FormGroup;
  constructor(private _fb: FormBuilder, private _router: Router) { }

  ngOnInit() {
    this.form = this._fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

  }
login( user: IUser) {

  console.log('Vous avez saisi le user :' + user.username);
  this._router.navigateByUrl('/')
}
}
