import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/services/products.service';
import { IProduct } from 'src/app/domain/iproduct';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-products-detail',
  templateUrl: './products-detail.component.html',
  styleUrls: ['./products-detail.component.scss']
})
export class ProductsDetailComponent implements OnInit {
  id: string ;
  produit: IProduct;
  constructor(private _service: ProductsService, private _route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this._route.snapshot.paramMap.get('id');
    this._service.getProductById(this.id).subscribe(
      response => this.produit = response,
    // tslint:disable-next-line:no-unused-expression
    erreur => console.log(`Attention, il ya erreur ${erreur}`)
    );
  }

}
