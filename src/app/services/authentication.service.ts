import { Injectable } from '@angular/core';
import { IUser } from '../domain/iuser';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  loginIn: boolean = false;
  constructor() { }

  authenticate(user: IUser): string {
    if ( user.username === 'toto' && user.password === 'password') {
      this.loginIn = true;
      const token:  string = '000AAA TOKEN 0000';
      localStorage.setItem('TOKEN', token);
      return token;
    }
  }
  logout() {
    if (localStorage.getItem('TOKEN')) {
      this.loginIn = false;
       localStorage.removeItem('TOKEN');
    }
  }

  isAuthenticated(): boolean {
    return this.loginIn;
  }
}
