import { Injectable } from '@angular/core';
import { IProduct } from '../domain/iproduct';
import {  HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private _HTTP: HttpClient) { }

  getAllProducts(): Observable<IProduct[]> {
    return this._HTTP.get<IProduct[]>(environment.URL_Fake);
  }
  getProductById(id: string): Observable<IProduct> {
    return this._HTTP.get<IProduct>(environment.URL_Fake + '/' + id);
  }
  addProduct(prod: IProduct): Observable<IProduct> {
    return this._HTTP.post<IProduct>(environment.URL_Fake, prod);
  }
}
